"use strict";

var viewportMeta = document.querySelector('meta[name = "viewport"]');
var viewportBtn = document.getElementById('viewport');
var viewportValue = 'width=device-width, initial-scale=1';

viewportBtn.addEventListener('click', function () {
    //console.log(viewportMeta);
    if (viewportMeta.content === '') {
        viewportMeta.content = viewportValue;
    } else {
        viewportMeta.content = '';
    }
    //viewportMeta.content === "" ? viewportMeta.content = viewportValue : viewportMeta.content = "";
});

console.log('Device pixel ratio: ' + window.devicePixelRatio);
