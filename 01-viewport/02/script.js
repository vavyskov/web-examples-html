"use strict";

var head = document.querySelector('head');
var viewportMeta = document.querySelector('meta[name="viewport"]');
//var viewportMeta = document.createElement('meta');
var viewportBtn = document.getElementById('viewport');

var maxWidthBtn = document.getElementById('maxWidth');
var style = document.createElement('style');

function setViewport() {
    if(viewport){
        //head.prepend(viewportMeta);
        //head.appendChild(viewportMeta);

        viewportMeta.setAttribute(
            "content",
            "width=device-width, initial-scale=1"
        );
    } else {
        //viewportMeta.remove();

        viewportMeta.setAttribute(
            "content",
            ""
        );
    }
}

function setMaxWidth() {
    if(maxWidth){
        style.innerHTML = `
            .box {
                max-width: 100%;
            }
        `;
        document.head.appendChild(style);
    } else {
        style.remove();
    }
}

var viewport = false; // Default value
viewportBtn.addEventListener('click', function () {
    viewport ? viewport = false : viewport = true;

    setViewport();
});

var maxWidth = false; // Default value
maxWidthBtn.addEventListener('click', function () {
    maxWidth ? maxWidth = false : maxWidth = true;

    setMaxWidth();
});

/*
document.getElementById("viewport").setAttribute(
    "content",
    "width=1024, initial-scale=0, maximum-scale=1.0, minimum-scale=0.25, user-scalable=yes"
);

document.getElementById("viewport").setAttribute(
    "content",
    "width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
);

document.querySelector('meta[name="viewport"]').setAttribute(
    "content",
    "width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
);

document.head.querySelector('meta[name="viewport"]').content = _viewport
*/
