"use strict";

var style = document.createElement('style');
var responsiveBtn = document.getElementById('responsive');
var responsive = false;

function setResponsive() {
    if (responsive) {
        style.innerHTML = `
            .box {
                max-width: 100%;
            }
        `;
        document.head.appendChild(style);
    } else {
        style.remove();
    }
}

responsiveBtn.addEventListener('click', function () {
    responsive ? responsive = false : responsive = true;
    setResponsive();
});
