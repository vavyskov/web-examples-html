(() => {
    document.querySelectorAll('summary').forEach(function (item) {
        item.addEventListener('click', function () {
            if (this.parentNode.hasAttribute('open')) {
                this.parentElement.querySelector('.menu').classList.add('details-close');
                this.parentElement.querySelector('.menu').classList.remove('details-open');
            } else {
                this.parentElement.querySelector('.menu').classList.add('details-open');
                this.parentElement.querySelector('.menu').classList.remove('details-close');
            }
        });
    });

    // document.querySelectorAll('details').forEach(function (detail) {
    //     detail.addEventListener('toggle', function () {
    //         this.querySelector('ul').style.transform = this.hasAttribute('open') ? 'scaleY(1)' : 'scaleY(0)';
    //     });
    // });

})();
